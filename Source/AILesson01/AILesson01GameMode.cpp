// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "AILesson01GameMode.h"
#include "AILesson01Character.h"
#include "UObject/ConstructorHelpers.h"

AAILesson01GameMode::AAILesson01GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
